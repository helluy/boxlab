// launch with cargo run --example basic

use boxlab::boite::*;

use std::io::Error;
use std::time::Instant;

fn main() -> Result<(), Error> {
    let xmin = [0., 0., 0.];
    let xmax = [1., 1., 0.1];
    let n = 100;
    let nxyz = [n, n, 1];
    let mut b = Boite::new(xmin, xmax, nxyz, 0, [0, 0, 0]);
    let now = Instant::now();
    b.init_data();

    b.solve_lbm(0.25);
    b.reduce();
    b.sauv_gmsh(0, "basic.msh")?;
    let t = now.elapsed().as_secs();
    println!("{}", t);

    Ok(())
}
