use std::io::Error;

#[test]
fn test1() -> Result<(), Error> {
    use boxlab::boite::*;

    let xmin = [0., 0., 0.];
    let xmax = [1., 1., 1.];
    let nxyz = [1, 1, 1];

    let mut b = Boite::new(xmin, xmax, nxyz, 0, [0, 0, 0]);
    b.reduce();
    b.sauv_gmsh(0, "test1.msh")
}

#[test]
fn test_2boxes() -> Result<(), Error> {
    use boxlab::boite::LAMBDA;
    use boxlab::boite::*;
    //use std::io::Error;
    use std::time::Instant;
    let n = 10;

    let xmin = [0.0, 0.0, 0.0];
    let xmax = [0.5, 1.0, 0.05];
    let nxyz = [n, 2 * n, 1];
    let mut b0 = Boite::new(xmin, xmax, nxyz, 0, [0, 0, 0]);

    let dx = [
        (xmax[0] - xmin[0]) / nxyz[0] as f64,
        (xmax[1] - xmin[1]) / nxyz[1] as f64,
        (xmax[2] - xmin[2]) / nxyz[2] as f64,
    ];

    let xmin = [0.5, 0.0, 0.0];
    let xmax = [1.0, 1.0, 0.05];
    let nxyz = [n, 2 * n, 1];
    let mut b1 = Boite::new(xmin, xmax, nxyz, 0, [0, 0, 0]);

    b0.init_data();
    b1.init_data();

    let dt = dx[0] / LAMBDA;
    assert!(
        (dx[0] - dx[1]).abs() <= f64::EPSILON,
        "dx and dy must be equal"
    );
    let mut t = 0.;
    let tmax = 0.25;
    while t < tmax {
        t += dt;
        println!("t={} dt={}", t, dt);
        b1.get_boundary_dir(0, Some(&b0));
        b1.get_boundary_dir(1, Some(&b0));
        b1.get_boundary_dir(2, None);
        b1.get_boundary_dir(3, None);
        b0.get_boundary_dir(0, Some(&b1));
        b0.get_boundary_dir(1, Some(&b1));
        b0.get_boundary_dir(2, None);
        b0.get_boundary_dir(3, None);
        b0.shift_data();
        b1.shift_data();
        b0.relax();
        b1.relax();
    }

    let now = Instant::now();
    b0.reduce();
    b1.reduce();
    b0.sauv_gmsh(0, "b0.msh")?;
    b1.sauv_gmsh(0, "b1.msh")?;
    let t = now.elapsed().as_secs();
    println!("{}", t);

    Ok(())
}
