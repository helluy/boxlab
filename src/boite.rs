extern crate ndarray;
use ndarray::parallel::prelude::*;
use ndarray::Array;
use ndarray::Array3;
use ndarray::Ix5;
//use rayon::iter::ParallelBridge;

use std::fs::File;
use std::io::BufWriter;
use std::io::{Error, Write};

/// Number of conservative variables
pub const M: usize = 1;

/// Number of kinetic direction (4 for 2D and 6 for 3D)
pub const K: usize = 4;

/// Kinetic velocity directions
const DIRI: [[i32; 3]; K] = [[-1, 0, 0], [1, 0, 0], [0, -1, 0], [0, 1, 0]];

/// Kinetic velocity scaling
pub const LAMBDA: f64 = 3.;

/// Length of the computational domain
const L: f64 = 1.;

/// Transport velocity
const VEL: [f64; 3] = [-1., 1., 0.];

/// A function with a peak shape
fn peak(x: [f64; 3]) -> f64 {
    let r = x[0] * x[0] + x[1] * x[1] + x[2] * x[2];
    let r = -30. * r;
    r.exp()
}

/// Exact solution of the transport eq. at velocity VEL
fn exact_sol(x: [f64; 3], t: f64) -> [f64; M] {
    let y = [x[0] - VEL[0] * t - L / 2., x[1] - VEL[1] * t - L / 2., 0.];
    [peak(y)]
}

/// Physical flux of the conservative transport equation
fn flux_phy(w: [f64; M], vnorm: [f64; 3]) -> [f64; M] {
    let v = VEL;
    let vdotn = v[0] * vnorm[0] + v[1] * vnorm[1] + v[2] * vnorm[2];
    [w[0] * vdotn]
}

/// Computation of the equilibrium kinetic distribution
fn w2f(w: [f64; M]) -> [[f64; M]; K] {
    let mut f = [[0.; M]; K];
    for (fd, vni) in f.iter_mut().zip(DIRI.iter()) {
        let vn = [vni[0] as f64, vni[1] as f64, vni[2] as f64];
        let flux = flux_phy(w, vn);
        for ((fd, w), flux) in fd.iter_mut().zip(w.iter()).zip(flux.iter()) {
            *fd = w / 4. + flux / 2. / LAMBDA;
            //fd[iv] = w[iv] / 4. + flux[iv] / 2. / LAMBDA;
        }
    }
    f
}

/// get macro conservative data from the kinetic ones
fn f2w(f: [[f64; M]; K]) -> [f64; M] {
    let mut w = [0.; M];
    for fd in f.iter() {
        for (w, fd) in w.iter_mut().zip(fd.iter()) {
            *w += fd;
        }
    }
    w
}

/// A "boite" (box) contains data and elementary geometrical informations
#[derive(Debug)]
pub struct Boite {
    m: usize,
    index: usize,
    i3: [usize; 3],
    data: Array<f64, Ix5>,
    data_next: Array<f64, Ix5>,
    voisin: [i32; K],
    xmin: [f64; 3],
    xmax: [f64; 3],
    n: [usize; 3],
}

/// A collection of Boites, arranged in a (i,j,k) fashion
#[derive(Debug)]
pub struct MultiBoite {
    xmin: [f64; 3],
    xmax: [f64; 3],
    nb: [usize; 3],
    raf: usize,
    pub boitev: Vec<Boite>,
}

impl MultiBoite {
    /// Returns a new MultiBoite from the bounding box
    /// (xmin, xmax), a refinement in each direction
    pub fn new(raf: usize, xmin: [f64; 3], xmax: [f64; 3], nb: [usize; 3]) -> MultiBoite {
        let dx = [
            (xmax[0] - xmin[0]) / nb[0] as f64,
            (xmax[1] - xmin[1]) / nb[1] as f64,
            (xmax[2] - xmin[2]) / nb[2] as f64,
        ];

        /// a box counter with k,j,i in this order
        /// for getting the good lexicographic order
        #[derive(Debug, PartialEq, PartialOrd, Eq, Ord)]
        struct Boxcount {
            even_odd: i32,
            k: usize,
            j: usize,
            i: usize,
        }

        let create_boxcount = |i, j, k| {
            let even_odd = (i + j + k) as i32 % 2;
            Boxcount { even_odd, k, j, i }
        };

        let mut white: Vec<Boxcount> = Vec::new();

        for k in 0..nb[2] {
            for j in 0..nb[1] {
                for i in 0..nb[0] {
                    white.push(create_boxcount(i, j, k));
                }
            }
        }

        white.sort();

        let white_count = white.iter().filter(|bc| bc.even_odd == 0).count();
        let black_count = white.iter().filter(|bc| bc.even_odd == 1).count();

        assert_eq!(white_count + black_count, white.len());
        assert_eq!(white_count, black_count);

        let black = white.split_off(white_count);

        let create_color_boite = |color: i32, ib: usize| {
            assert!(color == 0 || color == 1);

            let bc = if color == 0 { &white[ib] } else { &black[ib] };

            let i = [bc.i, bc.j, bc.k];

            let a = [
                xmin[0] + i[0] as f64 * dx[0],
                xmin[1] + i[1] as f64 * dx[1],
                xmin[2] + i[2] as f64 * dx[2],
            ];
            let b = [
                xmin[0] + (i[0] + 1) as f64 * dx[0],
                xmin[1] + (i[1] + 1) as f64 * dx[1],
                xmin[2] + (i[2] + 1) as f64 * dx[2],
            ];

            let nx = raf;
            let n = [nx, nx, 1];

            let index = i[0] + i[1] * nb[0] + i[2] * nb[0] * nb[1];

            let mut vois = [0; K];
            for (ik, dir) in DIRI.iter().enumerate() {
                let id = [
                    (((i[0] + nb[0]) as i32 - dir[0]) % nb[0] as i32) as usize,
                    (((i[1] + nb[1]) as i32 - dir[1]) % nb[1] as i32) as usize,
                    (((i[2] + nb[2]) as i32 - dir[2]) % nb[2] as i32) as usize,
                ];
                let bcd = Boxcount {
                    even_odd: 1 - color,
                    k: id[2],
                    j: id[1],
                    i: id[0],
                };
                let ibd = if color == 0 {
                    black.binary_search(&bcd)
                } else {
                    white.binary_search(&bcd)
                };
                vois[ik] = ibd.unwrap() as i32;
            }

            let mut bte = Boite::new(a, b, n, index, i);
            bte.set_voisins(vois);
            bte
        };

        let create_boite = |(i, j, k)| {
            let i = [i, j, k];

            let a = [
                xmin[0] + i[0] as f64 * dx[0],
                xmin[1] + i[1] as f64 * dx[1],
                xmin[2] + i[2] as f64 * dx[2],
            ];
            let b = [
                xmin[0] + (i[0] + 1) as f64 * dx[0],
                xmin[1] + (i[1] + 1) as f64 * dx[1],
                xmin[2] + (i[2] + 1) as f64 * dx[2],
            ];

            let nx = raf;
            let n = [nx, nx, 1];

            let index = i[1] + i[0] * nb[1] + i[2] * nb[0] * nb[1];

            Boite::new(a, b, n, index, i)
        };

        let _boite = Array3::from_shape_fn(nb, create_boite);

        let mut boitev: Vec<Boite> = Vec::new();

        for (ib, _bc) in white.iter().enumerate() {
            let bte = create_color_boite(0, ib);
            boitev.push(bte);
        }

        for (ib, _bc) in black.iter().enumerate() {
            let bte = create_color_boite(1, ib);
            boitev.push(bte);
        }

        MultiBoite {
            xmin,
            xmax,
            nb,
            raf,
            boitev,
        }
    }

    /// From the 3-index of a box and a direction
    /// returns the 3-index of the neighbour in the opposite direction
    #[allow(dead_code)]
    fn vois(&self, dir: [i32; 3], i: [usize; 3]) -> [usize; 3] {
        [
            ((i[0] as i32 + dir[0]) % self.nb[0] as i32) as usize,
            ((i[1] as i32 + dir[1]) % self.nb[1] as i32) as usize,
            ((i[2] as i32 + dir[2]) % self.nb[2] as i32) as usize,
        ]
    }

    pub fn init_data(&mut self) {
        self.boitev.par_iter_mut().for_each(|b| b.init_data());
    }

    /// Exchange the boundary data between all the boites in
    /// the multiboite
    pub fn collect(&mut self) {
        // for the moment, only 2D calculations...
        assert_eq!(self.nb[2], 1);
        // only works for an even number of boxes in the direction
        let n = self.nb[0];
        assert_eq!(n % 2, 0);
        // the exchanges are split bewteen even and odd index
        // in this way, when we mutate white boxes, we know that black boxes are  not changed
        // and vice versa

        let white_count = self
            .boitev
            .iter()
            .filter(|b| {
                let i = b.i3;
                let s = i[0] + i[1] + i[2];
                let even_odd = s % 2;
                even_odd == 0
            })
            .count();

        let (white_boite, black_boite) = self.boitev.split_at_mut(white_count);

        // for bte in white_boite.iter_mut().par_bridge() {
        //     for dir in 0..K {
        //         let ibd = bte.voisin[dir] as usize;
        //         let bted = &black_boite[ibd];
        //         bte.get_boundary_dir(dir, Some(&bted));
        //     }
        // }

        white_boite.par_iter_mut().for_each(|bte| {
            for dir in 0..K {
                let ibd = bte.voisin[dir] as usize;
                let bted = &black_boite[ibd];
                bte.get_boundary_dir(dir, Some(&bted));
            }
        });

        black_boite.par_iter_mut().for_each(|bte| {
            for dir in 0..K {
                let ibd = bte.voisin[dir] as usize;
                let bted = &white_boite[ibd];
                bte.get_boundary_dir(dir, Some(&bted));
            }
        });
    }

    /// Shift data on all the boxes
    pub fn shift_data(&mut self) {
        self.boitev.par_iter_mut().for_each(|b| b.shift_data());
    }

    /// Relax data on all the boxes
    pub fn relax(&mut self) {
        self.boitev.par_iter_mut().for_each(|b| b.relax());
    }

    /// Sum all the kinetic data in kinetic data ik=0
    pub fn reduce(&mut self) {
        self.boitev.par_iter_mut().for_each(|b| b.reduce());
    }

    /// Solve the equation with the LBM method up to t=tmax
    pub fn solve_lbm(&mut self, tmax: f64) {
        println!("LBM time steps...");
        let dx = [
            (self.xmax[0] - self.xmin[0]) / self.nb[0] as f64,
            (self.xmax[1] - self.xmin[1]) / self.nb[1] as f64,
            (self.xmax[2] - self.xmin[2]) / self.nb[2] as f64,
        ];
        let dt = dx[0] / LAMBDA / self.boitev[0].n[0] as f64;
        assert!(
            (dx[0] - dx[1]).abs() <= f64::EPSILON,
            "dx and dy must be equal"
        );
        let mut t = 0.;
        while t < tmax {
            t += dt;
            println!("t={:.8} dt={:.8}", t, dt);
            self.collect();
            self.shift_data();
            self.relax();
        }
    }

    /// Generate a gmsh file for each box in the multibox
    pub fn sauv_gmsh(&self) -> Result<(), Error> {
        println!("Create gmsh files...");
        let res = Ok(());
        // parallel generation of the meshes !
        self.boitev.par_iter().for_each(|b| {
            let mut mbxx = String::from("mb");
            let num = format!("{:04}", b.index);
            println!("{}", num);
            mbxx.push_str(&num);
            mbxx.push_str(".msh");
            let _res = b.sauv_gmsh(0, &mbxx);
        });
        res
    }
}

impl Boite {
    /// Returns a new Boite from the bounding box
    /// (xmin, xmax), a refinment in each direction
    pub fn new(
        xmin: [f64; 3],
        xmax: [f64; 3],
        n: [usize; 3],
        index: usize,
        i3: [usize; 3],
    ) -> Boite {
        // additional lines for applying boundary condition
        // indices i=0 and i=n+1 are the boundaries and
        // are shared with the neighbours
        let np = [n[0] + 2, n[1] + 2, n[2] + 2];
        let data = Array::from_shape_fn((M, K, np[2], np[1], np[0]), |(_iv, _ik, _k, _j, _i)| {
            0 as f64
        });
        let data_next =
            Array::from_shape_fn((M, K, np[2], np[1], np[0]), |(_iv, _ik, _k, _j, _i)| {
                0 as f64
            });
        let vois = [-1; K];
        Boite {
            m: M,
            index,
            i3,
            data,
            voisin: vois,
            data_next,
            xmin,
            xmax,
            n,
        }
    }

    /// # Examples
    /// ```
    /// use boxlab::boite::Boite;
    /// use boxlab::boite::K;
    /// let xmin = [0., 0., 0.];
    /// let xmax = [1., 1., 0.1];
    /// let nxyz = [2, 2, 1];
    /// let mut b = Boite::new(xmin, xmax, nxyz, 0, [0, 0, 0]);
    /// b.set_voisins([-1; K]);
    /// b.init_data();  
    /// ```

    /// set the index of the neighbor cells for all the K
    /// directions.
    pub fn set_voisins(&mut self, vois: [i32; K]) {
        self.voisin = vois;
    }

    /// Apply the exact solution at t=0 on the box
    pub fn init_data(&mut self) {
        let dx = [
            (self.xmax[0] - self.xmin[0]) / self.n[0] as f64,
            (self.xmax[1] - self.xmin[1]) / self.n[1] as f64,
            (self.xmax[2] - self.xmin[2]) / self.n[2] as f64,
        ];

        for ((iv, ik, k, j, i), v) in self.data.indexed_iter_mut() {
            let x = [
                self.xmin[0] + i as f64 * dx[0] - dx[0] / 2.,
                self.xmin[1] + j as f64 * dx[1] - dx[1] / 2.,
                self.xmin[2] + k as f64 * dx[2] - dx[2] / 2.,
            ];
            let f = w2f(exact_sol(x, 0.)); // assez clean, mais pas efficace...
            *v = f[ik][iv as usize];
            //*v = ((self.i3[0] + self.i3[1] + self.i3[2]) as i32 % 2) as f64 / 4.;
        }
    }

    /// Apply periodic boundary condition on the box
    /// (may be used in some tests of for debug)
    pub fn get_boundary_data(&mut self) {
        for ik in 0..K {
            self.get_boundary_dir(ik, None);
        }
    }

    /// Get boundary data from a neighbour box along direction ik
    pub fn get_boundary_dir(&mut self, ik: usize, bvois: Option<&Boite>) {
        // first, get the bounds of the copy range
        let mut smin = [0 as usize, 0 as usize, 0 as usize];
        let mut smax = [0 as usize, 0 as usize, 0 as usize];
        let mut tmin = [0 as usize, 0 as usize, 0 as usize];
        let mut tmax = [0 as usize, 0 as usize, 0 as usize];
        let n = [self.n[0], self.n[1], self.n[2]];
        let dir = DIRI[ik];
        for d in 0..3 {
            if dir[d] == 0 {
                smin[d] = 1;
                smax[d] = n[d] + 1;
                tmin[d] = 1;
                tmax[d] = n[d] + 1;
            } else if dir[d] == 1 {
                smin[d] = 0;
                smax[d] = 1;
                tmin[d] = n[d];
                tmax[d] = n[d] + 1;
            } else {
                smin[d] = n[d] + 1;
                smax[d] = n[d] + 2;
                tmin[d] = 1;
                tmax[d] = 2;
                assert_eq!(dir[d], -1);
            }
        }

        // receive region of the array
        let mut srecv = self.data.slice_mut(ndarray::s![
            0..M,
            ik..ik + 1,
            smin[2]..smax[2],
            smin[1]..smax[1],
            smin[0]..smax[0]
        ]);
        match bvois {
            None => {
                for iv in 0..M {
                    let dir = DIRI[ik];
                    //println!("dir={:?}",dir);
                    for k in smin[2]..smax[2] {
                        let kp = k as i32 + dir[2] * n[2] as i32;
                        for j in smin[1]..smax[1] {
                            let jp = j as i32 + dir[1] * n[1] as i32;
                            for i in smin[0]..smax[0] {
                                let ip = i as i32 + dir[0] * n[0] as i32;
                                let kp = kp as usize;
                                let jp = jp as usize;
                                let ip = ip as usize;
                                self.data[(iv, ik, k, j, i)] = self.data[(iv, ik, kp, jp, ip)];
                            }
                        }
                    }
                }
                //panic!("pas encore programmé...");
                // forbidden
                // for ((iv, ik, k, j, i), data) in srecv.indexed_iter_mut() {
                //     let kp = k as i32 + dir[2] * n[2] as i32;
                //     let jp = j as i32 + dir[1] * n[1] as i32;
                //     let ip = i as i32 + dir[0] * n[0] as i32;

                //     let kp = kp as usize;
                //     let jp = jp as usize;
                //     let ip = ip as usize;
                //     *data = self.data[(iv, ik, kp, jp, ip)]; // two mutable borrows..
                //}
            }
            Some(b) => {
                // source boundary array
                let ssend = b.data.slice(ndarray::s![
                    0..M,
                    ik..ik + 1,
                    tmin[2]..tmax[2],
                    tmin[1]..tmax[1],
                    tmin[0]..tmax[0]
                ]);

                // actual copy
                srecv.assign(&ssend);
            }
        }
    }

    /// Shift data on a box for all the kinetic directions
    pub fn shift_data(&mut self) {
        use ndarray::s;

        for (ik, dir) in DIRI.iter().enumerate() {
            // interior slice
            let (imin, imax) = (1, self.n[0] + 1);
            let (jmin, jmax) = (1, self.n[1] + 1);
            let (kmin, kmax) = (1, self.n[2] + 1);
            let mut snext =
                self.data_next
                    .slice_mut(s![0..M, ik..ik + 1, kmin..kmax, jmin..jmax, imin..imax]);
            // shifted slice
            let (imin, imax) = (1 - dir[0], self.n[0] as i32 + 1 - dir[0]);
            let (jmin, jmax) = (1 - dir[1], self.n[1] as i32 + 1 - dir[1]);
            let (kmin, kmax) = (1 - dir[2], self.n[2] as i32 + 1 - dir[2]);
            let snow = self
                .data
                .slice(s![0..M, ik..ik + 1, kmin..kmax, jmin..jmax, imin..imax]);
            // copy the shifted slice to the interior slice
            snext.assign(&snow);
        }
    }
    #[allow(dead_code)]
    pub fn update(&mut self) {
        for (((_iv, _ik, _k, _j, _i), data), data_next) in
            self.data.indexed_iter_mut().zip(self.data_next.iter())
        {
            *data = *data_next;
        }
    }

    /// Second order LBM relaxation
    /// reduce om for lower order
    pub fn relax(&mut self) {
        for k in 1..self.n[2] + 1 {
            for j in 1..self.n[1] + 1 {
                for i in 1..self.n[0] + 1 {
                    let mut fnow = [[0. as f64; M]; K];
                    for (ik, fnow) in fnow.iter_mut().enumerate() {
                        for (iv, fnow) in fnow.iter_mut().enumerate() {
                            *fnow = self.data_next[(iv, ik, k, j, i)];
                        }
                    }
                    let w = f2w(fnow);
                    let feq = w2f(w);
                    let om = 2.;
                    for (ik, fnow) in fnow.iter().enumerate() {
                        for (iv, fnow) in fnow.iter().enumerate() {
                            self.data[(iv, ik, k, j, i)] = om * feq[ik][iv] + (1. - om) * *fnow;
                        }
                    }
                }
            }
        }
    }

    /// LBM algorithm on a single box with periodic boundary conditions
    /// (used in some tests)
    pub fn solve_lbm(&mut self, tmax: f64) {
        let dx = [
            (self.xmax[0] - self.xmin[0]) / self.n[0] as f64,
            (self.xmax[1] - self.xmin[1]) / self.n[1] as f64,
            (self.xmax[2] - self.xmin[2]) / self.n[2] as f64,
        ];
        let dt = dx[0] / LAMBDA;
        assert!(
            (dx[0] - dx[1]).abs() <= f64::EPSILON,
            "dx and dy must be equal"
        );
        let mut t = 0.;
        while t < tmax {
            t += dt;
            println!("t={} dt={}", t, dt);
            self.get_boundary_data();
            self.shift_data();
            //self.update();
            self.relax();
        }
    }

    /// Get conservative data in the first kinetic data
    /// (for plotting)
    pub fn reduce(&mut self) {
        for iv in 0..M {
            for ik in 1..K {
                for k in 0..self.n[2] + 2 {
                    for j in 0..self.n[1] + 2 {
                        for i in 0..self.n[0] + 2 {
                            self.data[(iv, 0, k, j, i)] += self.data[(iv, ik, k, j, i)];
                            //println!("ik={} v={}",ik,self.data[(iv, 0, k, j, i)]);
                        }
                    }
                }
            }
        }
    }

    /// Generate a gmsh view for kinetic data ik0
    /// # Examples
    /// ```
    /// use boxlab::boite::*;
    /// use std::io::{Error};
    /// let xmin = [0., 0., 0.];
    /// let xmax = [1., 1., 0.1];
    /// let nxyz = [10, 10, 1];
    /// let mut b = Boite::new(xmin, xmax, nxyz, 0,[0,0,0]);
    /// b.sauv_gmsh(0, "exgmsh.msh");
    /// ```
    pub fn sauv_gmsh(&self, ik0: usize, meshfilename: &str) -> Result<(), Error> {
        let meshfile = File::create(meshfilename)?;
        let mut meshfile = BufWriter::new(meshfile); // create a buffer for faster writes...

        write!(meshfile, "$MeshFormat\n4.1 0 8\n$EndMeshFormat\n")?;
        writeln!(meshfile, "$Nodes")?;

        assert!(ik0 < K, "Kinetic index out of range");

        let dx = [
            (self.xmax[0] - self.xmin[0]) / self.n[0] as f64,
            (self.xmax[1] - self.xmin[1]) / self.n[1] as f64,
            (self.xmax[2] - self.xmin[2]) / self.n[2] as f64,
        ];

        use ndarray::s;
        let dec = 1; // 1: remove borders 0: with borders
        let ss = self.data.slice(s![
            0,
            ik0,
            1..2,
            dec..self.n[1] + 2 - dec,
            dec..self.n[0] + 2 - dec
        ]);

        let it = ss.indexed_iter();

        let xc = [
            [0., 0., 0.],
            [1., 0., 0.],
            [1., 1., 0.],
            [0., 1., 0.],
            [0., 0., 1.],
            [1., 0., 1.],
            [1., 1., 1.],
            [0., 1., 1.],
        ];

        let mut count = 0;
        let nv = ss.len() * 8;
        writeln!(meshfile, "1 {} 1 {}", nv, nv)?;
        writeln!(meshfile, "3 1 0 {}", nv)?;
        for ((_k, _j, _i), _v) in it {
            for _ic in 0..8 {
                count += 1;
                writeln!(meshfile, "{}", count)?;
            }
        }
        let it = ss.indexed_iter();
        for ((k, j, i), _v) in it {
            for xyz in &xc {
                let dec = 1 - dec;
                let x = [
                    self.xmin[0] + (i as f64 - dec as f64) * dx[0] + dx[0] * xyz[0],
                    self.xmin[1] + (j as f64 - dec as f64) * dx[1] + dx[1] * xyz[1],
                    self.xmin[2] + (k as f64 - dec as f64) * dx[2] + dx[2] * xyz[2],
                ];
                count += 1;
                writeln!(meshfile, "{} {} {}\n", x[0], x[1], x[2])?;
            }
        }
        writeln!(meshfile, "$EndNodes\n$Elements")?;
        let mut count = 0;
        //let mut iv = 0;
        let nv = ss.len();
        writeln!(meshfile, "1 {} 1 {}", nv, nv)?;
        writeln!(meshfile, "3 1 5 {}", nv)?;
        let it = ss.indexed_iter();
        for (iv, ((_k, _j, _i), _v)) in it.enumerate() {
            //iv += 1;
            write!(meshfile, "{} ", iv)?;
            for _ic in 0..8 {
                count += 1;
                write!(meshfile, "{} ", count)?;
            }
            writeln!(meshfile)?;
        }
        writeln!(meshfile, "$EndElements")?;
        writeln!(meshfile, "$NodeData")?;
        writeln!(meshfile, "1\n\"Boxlab data\"")?;
        writeln!(meshfile, "1")?; // one time
        writeln!(meshfile, "0.0")?; // time
        writeln!(meshfile, "3")?; // 3 integers follow
        writeln!(meshfile, "0\n1\n{}", 8 * nv)?; // time step 0, one component, nv
        let mut count = 0;
        let it = ss.indexed_iter();
        for ((_k, _j, _i), v) in it {
            for _ic in 0..8 {
                count += 1;
                writeln!(meshfile, "{} {}", count, v)?;
            }
        }

        writeln!(meshfile, "$EndNodeData")?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::boite::Boite;
    use std::io::Error;
    use std::time::Instant;
    #[test]
    fn test1() -> Result<(), Error> {
        let xmin = [0., 0., 0.];
        let xmax = [1., 1., 0.];
        let nxyz = [10, 10, 1];
        let mut b = Boite::new(xmin, xmax, nxyz, 0, [0, 0, 0]);
        let now = Instant::now();
        b.init_data();
        b.solve_lbm(0.5);
        b.reduce();
        b.sauv_gmsh(0, "test1.msh")?;
        let t = now.elapsed().as_secs();
        println!("{}", t);
        Ok(())
    }
    #[test]
    fn test2() -> Result<(), Error> {
        use crate::boite::Boite;
        let xmin = [0., 0., 0.];
        let xmax = [1., 1., 1.];
        let nxyz = [2, 2, 2];
        let b = Boite::new(xmin, xmax, nxyz, 0, [0, 0, 0]);
        b.sauv_gmsh(0, "test2.msh")
    }
    #[test]
    fn test3() -> Result<(), Error> {
        let xmin = [0., 0., 0.];
        let xmax = [1., 1., 0.1];
        let n = [2, 2, 1];
        use crate::boite::MultiBoite;
        let mb = MultiBoite::new(2, xmin, xmax, n);
        let nb = mb.vois([1, 0, 0], [0, 0, 0]);
        println!("2 %2 ={}", 2 % 2);
        println!("-1 %2 ={}", -1 % 2);
        assert_eq!(nb, [1, 0, 0]);
        let dir = [0, 1, 0];
        let nb = mb.vois(dir, [0, 1, 0]);
        assert_eq!(nb, [0, 0, 0]);
        Ok(())
    }
}
