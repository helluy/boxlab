pub mod boite;

use boxlab::boite::*;

use std::io::Error;

// command line:
// time RAYON_NUM_THREADS=8 cargo run --release

fn main() -> Result<(), Error> {
    let xmin = [0.0, 0.0, 0.0];
    let xmax = [1.0, 1.0, 0.05];
    let nb = [4, 4, 1];
    let raf = 200;
    let mut mb = MultiBoite::new(raf, xmin, xmax, nb);

    mb.init_data();

    mb.solve_lbm(1.1);

    mb.reduce();

    //mb.sauv_gmsh()?;

    Ok(())
}
